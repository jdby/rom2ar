module V1
  (rom2ar) where

import Data.Char

rom2ar :: String -> Maybe Integer
rom2ar s = do
  ints <- mapM toNum $ map toUpper s
  parse ints False 0

prefixes p n
  | p == 1 = n == 5 || n == 10
  | p == 10 = n == 50 || n == 100
  | p == 100 = n == 500 || n == 1000
  | otherwise = False

toNum :: Char -> Maybe Integer
toNum i = case i of
  'I' -> Just 1
  'V' -> Just 5
  'X' -> Just 10
  'L' -> Just 50
  'C' -> Just 100
  'D' -> Just 500
  'M' -> Just 1000
  otherwise -> Nothing

parse :: [Integer] -> Bool -> Integer -> Maybe Integer
parse (x:[]) _ _ = Just x
parse (h:n:xs) prefixed groupCount
  | h < n && not prefixed && prefixes h n = do
      r <- parse (n:xs) True 0
      Just $ r - h
  | h == n && groupCount < 2 && not prefixed = do
      r <- parse (n:xs) False (groupCount+1)
      Just $ r + h
  | h > n = do
      r <- parse (n:xs) False 0
      Just $ r + h
  | otherwise = Nothing
