module V2
    ( rom2ar, rom2arm
    ) where

import Data.Char
import Text.ParserCombinators.Parsec (ParseError)
import Text.Parsec
import Text.Parsec.String (Parser)

data Numeral
  = I
  | V
  | X
  | L
  | C
  | D
  | M
  deriving (Show, Read, Eq, Ord)

data Unit
  = Prefixed Numeral Numeral -- e.g. IV or XC
  | Group Numeral Int -- e.g. III, or XX
  | Single Numeral -- e.g. M
  | MaxUnit
  | ZeroUnit
  deriving Show

groupable n = case n of
  I -> True
  X -> True
  C -> True
  M -> True
  otherwise -> False

-- get the list of Roman Numeral characters that p is a valid prefix for
prefixes p
  | p == I = "VX"
  | p == X = "LC"
  | p == C = "DM"
  | otherwise = ""

canFollow :: Unit -> Unit -> Bool
-- Single can follow a Single if it has the same or less digits (in arabic...) and has a smaller value
-- e.g. XV or VI, but not VV. XX would be a Group
canFollow u@(Single s1) n@(Single s2) = digits u >= digits n  && (nvalue s1) > (nvalue s2)
-- A Group can follow a single if it has the same or less digits (e.g. V and III), and has a smaller value
canFollow u@(Single sn) n@(Group gn _) = digits u >= digits n && (nvalue sn) > (nvalue gn)
-- anything else has to have less digits (e.g. 2 Prefixed values IXIV is not valid as they are both equal in digits (9 and 4 have 1 digit)
canFollow u n = digits u > digits n

digits :: Unit -> Int
digits MaxUnit = 5
digits u = 1 + (floor $ logBase 10 $ (fromIntegral (value u) :: Float))

-- convert a numeral into it's value
nvalue :: Numeral -> Int
nvalue I = 1
nvalue V = 5
nvalue X = 10
nvalue L = 50
nvalue C = 100
nvalue D = 500
nvalue M = 1000

-- convert a unit into it's value
value :: Unit -> Int
value (Single s) = nvalue s
value (Prefixed p n) = (nvalue n) - (nvalue p)
value (Group n i) = (nvalue n) * i
value MaxUnit = error "MaxUnit has no value"
value ZeroUnit = error "ZeroUnit has no value"

-- parsers:

romanDigit = oneOf "IVXLCDM"

numeral :: Parser Numeral
numeral = do
  c <- romanDigit
  return $ read [c]

numeralx :: Numeral -> Parser Numeral
numeralx n = do
  nx <- satisfy (\c -> c == (head $ show n))
  return $ read [nx]

prefixed :: Parser Unit
prefixed = do
  first <- numeral
  second <- oneOf $ prefixes first
  return $ Prefixed first $ read [second]

group :: Parser Unit
group = do
  first <- numeral
  rest <- many (numeralx first)
  let len = 1 + length rest
  if (len == 2 || len == 3) && groupable first
    then return $ Group first len
    else unexpected "Group requires 2 or 3 numerals of I,X,C or M"

single :: Parser Unit
single = do
  n <- numeral
  return $ Single n

unit :: Unit -> Parser Unit
unit u = do
  nxt <- try prefixed <|> try group <|> try single
  if canFollow u nxt
    then return nxt
    else unexpected "Unexpected format in Roman Numeral"

noUnit :: Parser Unit
noUnit = do
  eof
  return ZeroUnit

roman :: Unit -> Parser [Unit]
roman u = do
  fst <- try $ unit u <|> noUnit
  case fst of
    ZeroUnit -> return []
    otherwise -> do rst <- roman fst
                    return $ fst:rst

-- Sum the values of a list of units
sumRoman :: [Unit] -> Int
sumRoman us = foldl (+) 0 (map value us)

rom2ar :: String -> Either ParseError Int
rom2ar romanNumeral = do
  let upperRN = map toUpper romanNumeral
  r <- parse (roman MaxUnit) "" upperRN
  return $ sumRoman r

rom2arm :: String -> Maybe Int
rom2arm romanNumeral = do
  let res = rom2ar romanNumeral
  case res of
    Right i -> Just i
    Left _ -> Nothing
