module V1Spec (spec) where

import V1

import Test.Hspec

spec :: Spec
spec =
  describe "rom2ar" $ do
    it "Correctly parses individual numerals" $ do
      rom2ar "I" `shouldBe` Just 1 
      rom2ar "V" `shouldBe` Just 5 
      rom2ar "X" `shouldBe` Just 10 
      rom2ar "L" `shouldBe` Just 50 
      rom2ar "C" `shouldBe` Just 100 
      rom2ar "D" `shouldBe` Just 500 
      rom2ar "M" `shouldBe` Just 1000 
    it "Correctly parses simple prefixes" $ do
      rom2ar "IV" `shouldBe` Just 4 
      rom2ar "IX" `shouldBe` Just 9 
      rom2ar "XL" `shouldBe` Just 40 
      rom2ar "XC" `shouldBe` Just 90 
      rom2ar "CD" `shouldBe` Just 400 
      rom2ar "CM" `shouldBe` Just 900 
    it "Correctly parses groups" $ do
      rom2ar "II" `shouldBe` Just 2 
      rom2ar "III" `shouldBe` Just 3 
      rom2ar "XX" `shouldBe` Just 20 
      rom2ar "XXX" `shouldBe` Just 30 
      rom2ar "CC" `shouldBe` Just 200 
      rom2ar "CCC" `shouldBe` Just 300 
      rom2ar "MM" `shouldBe` Just 2000 
      rom2ar "MMM" `shouldBe` Just 3000
    it "Correctly parses consecutive single numerals" $ do
      rom2ar "VI" `shouldBe` Just 6
      rom2ar "XI" `shouldBe` Just 11 
      rom2ar "CI" `shouldBe` Just 101
      rom2ar "CX" `shouldBe` Just 110 
      rom2ar "MCX" `shouldBe` Just 1110 
      rom2ar "MV" `shouldBe` Just 1005
      rom2ar "MVI" `shouldBe` Just 1006
      rom2ar "MLI" `shouldBe` Just 1051
      rom2ar "MDI" `shouldBe` Just 1501
    it "Correctly parses complex numerals" $ do
      rom2ar "XVI" `shouldBe` Just 16
      rom2ar "XCIII" `shouldBe` Just 93 
      rom2ar "CXVIII" `shouldBe` Just 118
      rom2ar "MMMDCCCLXXXVIII" `shouldBe` Just 3888 
      rom2ar "MCMXC" `shouldBe` Just 1990 
    it "Fails to parse non roman numerals" $ do
      rom2ar "a" `shouldBe` Nothing
      rom2ar "b" `shouldBe` Nothing
      rom2ar "Xb" `shouldBe` Nothing
      rom2ar "Mb" `shouldBe` Nothing
      rom2ar "IXb" `shouldBe` Nothing
      rom2ar "MCMXCIIIb" `shouldBe` Nothing
    it "Fails to parse non-allowed prefixes" $ do
      rom2ar "IL" `shouldBe` Nothing
      rom2ar "IC" `shouldBe` Nothing
      rom2ar "ID" `shouldBe` Nothing
      rom2ar "IM" `shouldBe` Nothing
      rom2ar "VX" `shouldBe` Nothing
      rom2ar "VC" `shouldBe` Nothing
      rom2ar "VD" `shouldBe` Nothing
      rom2ar "VM" `shouldBe` Nothing
      rom2ar "XD" `shouldBe` Nothing
      rom2ar "XM" `shouldBe` Nothing
      rom2ar "LC" `shouldBe` Nothing
      rom2ar "LD" `shouldBe` Nothing
      rom2ar "LM" `shouldBe` Nothing
    it "Fails to parse groups of 5s" $ do
      rom2ar "VV" `shouldBe` Nothing
      rom2ar "VVV" `shouldBe` Nothing
      rom2ar "LL" `shouldBe` Nothing
      rom2ar "LLL" `shouldBe` Nothing
      rom2ar "DD" `shouldBe` Nothing
      rom2ar "DDD" `shouldBe` Nothing
    it "Fails to parse groups of length greater than 3" $ do
      rom2ar "IIII" `shouldBe` Nothing
      rom2ar "IIIII" `shouldBe` Nothing
      rom2ar "XXXX" `shouldBe` Nothing
      rom2ar "XXXXX" `shouldBe` Nothing
      rom2ar "CCCC" `shouldBe` Nothing
      rom2ar "CCCCC" `shouldBe` Nothing
      rom2ar "MMMM" `shouldBe` Nothing
      rom2ar "MMMMM" `shouldBe` Nothing
    it "Fails to parse prefix before single of same order" $ do
      rom2ar "IXI" `shouldBe` Nothing
      rom2ar "IXV" `shouldBe` Nothing
      rom2ar "IVV" `shouldBe` Nothing
      rom2ar "IVI" `shouldBe` Nothing
      rom2ar "XCL" `shouldBe` Nothing
      rom2ar "CMD" `shouldBe` Nothing
      rom2ar "IXI" `shouldBe` Nothing
      rom2ar "XCX" `shouldBe` Nothing
      rom2ar "CMC" `shouldBe` Nothing
    it "Fails to parse prefix before prefix of same order" $ do
      rom2ar "IXIV" `shouldBe` Nothing
      rom2ar "IVIV" `shouldBe` Nothing
      rom2ar "IXIX" `shouldBe` Nothing
      rom2ar "XCXL" `shouldBe` Nothing
      rom2ar "XCXC" `shouldBe` Nothing
      rom2ar "XLXC" `shouldBe` Nothing
      rom2ar "XLXL" `shouldBe` Nothing
      rom2ar "CMCD" `shouldBe` Nothing
      rom2ar "CMCM" `shouldBe` Nothing
      rom2ar "CDCD" `shouldBe` Nothing
      rom2ar "CDCM" `shouldBe` Nothing
      rom2ar "IXI" `shouldBe` Nothing
      rom2ar "XCX" `shouldBe` Nothing
      rom2ar "CMC" `shouldBe` Nothing
    it "Fails to parse other bad things" $ do
      rom2ar "ABCDE" `shouldBe` Nothing
      rom2ar "ICXXXXIIVV" `shouldBe` Nothing
      rom2ar "IIIV" `shouldBe` Nothing
      rom2ar "MCMXD" `shouldBe` Nothing
