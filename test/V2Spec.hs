module V2Spec (spec) where

import V2

import Test.Hspec

spec :: Spec
spec =
  describe "rom2arm" $ do
    it "Correctly parses individual numerals" $ do
      rom2arm "I" `shouldBe` Just 1 
      rom2arm "V" `shouldBe` Just 5 
      rom2arm "X" `shouldBe` Just 10 
      rom2arm "L" `shouldBe` Just 50 
      rom2arm "C" `shouldBe` Just 100 
      rom2arm "D" `shouldBe` Just 500 
      rom2arm "M" `shouldBe` Just 1000 
    it "Correctly parses simple prefixes" $ do
      rom2arm "IV" `shouldBe` Just 4 
      rom2arm "IX" `shouldBe` Just 9 
      rom2arm "XL" `shouldBe` Just 40 
      rom2arm "XC" `shouldBe` Just 90 
      rom2arm "CD" `shouldBe` Just 400 
      rom2arm "CM" `shouldBe` Just 900 
    it "Correctly parses groups" $ do
      rom2arm "II" `shouldBe` Just 2 
      rom2arm "III" `shouldBe` Just 3 
      rom2arm "XX" `shouldBe` Just 20 
      rom2arm "XXX" `shouldBe` Just 30 
      rom2arm "CC" `shouldBe` Just 200 
      rom2arm "CCC" `shouldBe` Just 300 
      rom2arm "MM" `shouldBe` Just 2000 
      rom2arm "MMM" `shouldBe` Just 3000
    it "Correctly parses consecutive single numerals" $ do
      rom2arm "VI" `shouldBe` Just 6
      rom2arm "XI" `shouldBe` Just 11 
      rom2arm "CI" `shouldBe` Just 101
      rom2arm "CX" `shouldBe` Just 110 
      rom2arm "MCX" `shouldBe` Just 1110 
      rom2arm "MV" `shouldBe` Just 1005
      rom2arm "MVI" `shouldBe` Just 1006
      rom2arm "MLI" `shouldBe` Just 1051
      rom2arm "MDI" `shouldBe` Just 1501
    it "Correctly parses complex numerals" $ do
      rom2arm "XVI" `shouldBe` Just 16
      rom2arm "XCIII" `shouldBe` Just 93 
      rom2arm "CXVIII" `shouldBe` Just 118
      rom2arm "MMMDCCCLXXXVIII" `shouldBe` Just 3888 
      rom2arm "MCMXC" `shouldBe` Just 1990 
    it "Fails to parse non roman numerals" $ do
      rom2arm "a" `shouldBe` Nothing
      rom2arm "b" `shouldBe` Nothing
      rom2arm "Xb" `shouldBe` Nothing
      rom2arm "Mb" `shouldBe` Nothing
      rom2arm "IXb" `shouldBe` Nothing
      rom2arm "XbV" `shouldBe` Nothing
      rom2arm "MCMXCIIIb" `shouldBe` Nothing
    it "Fails to parse non-allowed prefixes" $ do
      rom2arm "IL" `shouldBe` Nothing
      rom2arm "IC" `shouldBe` Nothing
      rom2arm "ID" `shouldBe` Nothing
      rom2arm "IM" `shouldBe` Nothing
      rom2arm "VX" `shouldBe` Nothing
      rom2arm "VC" `shouldBe` Nothing
      rom2arm "VD" `shouldBe` Nothing
      rom2arm "VM" `shouldBe` Nothing
      rom2arm "XD" `shouldBe` Nothing
      rom2arm "XM" `shouldBe` Nothing
      rom2arm "LC" `shouldBe` Nothing
      rom2arm "LD" `shouldBe` Nothing
      rom2arm "LM" `shouldBe` Nothing
    it "Fails to parse groups of 5s" $ do
      rom2arm "VV" `shouldBe` Nothing
      rom2arm "VVV" `shouldBe` Nothing
      rom2arm "LL" `shouldBe` Nothing
      rom2arm "LLL" `shouldBe` Nothing
      rom2arm "DD" `shouldBe` Nothing
      rom2arm "DDD" `shouldBe` Nothing
    it "Fails to parse groups of length greater than 3" $ do
      rom2arm "IIII" `shouldBe` Nothing
      rom2arm "IIIII" `shouldBe` Nothing
      rom2arm "XXXX" `shouldBe` Nothing
      rom2arm "XXXXX" `shouldBe` Nothing
      rom2arm "CCCC" `shouldBe` Nothing
      rom2arm "CCCCC" `shouldBe` Nothing
      rom2arm "MMMM" `shouldBe` Nothing
      rom2arm "MMMMM" `shouldBe` Nothing
    it "Fails to parse prefix before single of same order" $ do
      rom2arm "IXI" `shouldBe` Nothing
      rom2arm "IXV" `shouldBe` Nothing
      rom2arm "IVV" `shouldBe` Nothing
      rom2arm "IVI" `shouldBe` Nothing
      rom2arm "XCL" `shouldBe` Nothing
      rom2arm "CMD" `shouldBe` Nothing
      rom2arm "IXI" `shouldBe` Nothing
      rom2arm "XCX" `shouldBe` Nothing
      rom2arm "CMC" `shouldBe` Nothing
    it "Fails to parse prefix before prefix of same order" $ do
      rom2arm "IXIV" `shouldBe` Nothing
      rom2arm "IVIV" `shouldBe` Nothing
      rom2arm "IXIX" `shouldBe` Nothing
      rom2arm "XCXL" `shouldBe` Nothing
      rom2arm "XCXC" `shouldBe` Nothing
      rom2arm "XLXC" `shouldBe` Nothing
      rom2arm "XLXL" `shouldBe` Nothing
      rom2arm "CMCD" `shouldBe` Nothing
      rom2arm "CMCM" `shouldBe` Nothing
      rom2arm "CDCD" `shouldBe` Nothing
      rom2arm "CDCM" `shouldBe` Nothing
      rom2arm "IXI" `shouldBe` Nothing
      rom2arm "XCX" `shouldBe` Nothing
      rom2arm "CMC" `shouldBe` Nothing
    it "Fails to parse other bad things" $ do
      rom2arm "ABCDE" `shouldBe` Nothing
      rom2arm "ICXXXXIIVV" `shouldBe` Nothing
      rom2arm "IIIV" `shouldBe` Nothing
      rom2arm "MCMXD" `shouldBe` Nothing
