module Main where

import System.Environment (getArgs)
import V1
import V2

main :: IO ()
main = do
  args <- getArgs
  mainV1 $ head args
  mainV2 $ head args

mainV1 :: String -> IO ()
mainV1 rom = do
  let result = V1.rom2ar rom
  case result of
    Just i -> putStrLn $ "V1: " ++ show i
    Nothing -> putStrLn "Error parsing Roman Numeral"
  
mainV2 :: String -> IO ()
mainV2 rom = do
  let result = V2.rom2ar rom
  case result of
    Right i -> putStrLn $ "V2: " ++ show i
    Left e -> do
      putStrLn "Error parsing Roman Numeral:"
      putStrLn $ show e
