#!/bin/bash

while read -r line
do
    arabic=$(echo $line | awk '{print $1}')
    roman=$(echo $line | awk '{print $2}')
    output=$(stack run $roman)
    outputV1=$(echo $output | grep "V1" | awk '{print $2}')
    outputV2=$(echo $output | grep "V2" | awk '{print $2}')

    if [[ $outputV1 -ne $arabic ]]; then
        echo "V1 failed for $roman, expected $arabic, but got $outputV1"
    fi
    
    if [[ $outputV2 -ne $arabic ]]; then
        echo "V2 failed for $roman, expected $arabic, but got $outputV2"
    fi
done < test/numerals.txt 
