# rom2ar

Two implementations of a roman numeral to arabic converter

### To run:

    stack run <roman numeral>
    
This will output the result of both implementations

### To test:

(V1 implementation does not support all negative tests so some unit tests fail.)

unit tests:

    stack test

test all numbers < 3999 (this is slow...)

    ./test.sh
